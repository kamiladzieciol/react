import React from 'react'; 
import ReactDOM from 'react-dom';
import Header from './components/Header';
import Nav from './components/Nav';
import Main from './components/Main';
import Section from './components/Section';
import Footer from './components/Footer';
import Container from '@material-ui/core/Container';
import './components/Global.css';

const App = () => {
    return (
            <Container maxWidth="lg"> 
                <Header />
                <Nav />
                <Main />
                <Section />
                <Footer />
            </Container>
    );
};

ReactDOM.render(
  <App />,
    document.querySelector('#root')
);
