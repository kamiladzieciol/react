import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import DehazeIcon from '@material-ui/icons/Dehaze';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

  
const useStyles = makeStyles(theme => ({
    nav_bar: {
        backgroundColor: '#000',
        color: '#fff',
        minHeight: '50px',
    },
    nav_bar_list: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        maxHeight: '50px',
        padding: '0px',
    },
    nav_bar_link: {
            width: 'auto',
        },
    nav_bar_button: {
        textAlign: 'left',
        margin: '11px',
    }
}));


const Nav = () => {
    const addclass = useStyles();
    return (
        <Grid container className={addclass.nav_bar}>
            <Grid item xs={12}>
            <Hidden smUp>
                <Box component="div" className={addclass.nav_bar_button}> 
                    <DehazeIcon />
                </Box>
                </Hidden>
                <Hidden only="xs">
                    <List className={addclass.nav_bar_list}>
                        <ListItem className={addclass.nav_bar_link}><ListItemText>Lorem Ipsum</ListItemText></ListItem>
                        <ListItem className={addclass.nav_bar_link}><ListItemText>Lorem Ipsum</ListItemText></ListItem>
                        <ListItem className={addclass.nav_bar_link}><ListItemText>Lorem Ipsum</ListItemText></ListItem>
                        <ListItem className={addclass.nav_bar_link}><ListItemText>Lorem Ipsum</ListItemText></ListItem>
                        <ListItem className={addclass.nav_bar_link}><ListItemText>Lorem Ipsum</ListItemText></ListItem>
                        <ListItem className={addclass.nav_bar_link}><ListItemText>Lorem Ipsum</ListItemText></ListItem>
                        <ListItem className={addclass.nav_bar_link}><ListItemText>Lorem Ipsum</ListItemText></ListItem>
                    </List>
                </Hidden>
            </Grid>
        </Grid>
    ); 
};


export default Nav;
