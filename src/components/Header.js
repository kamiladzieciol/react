import React from 'react';
import Box from '@material-ui/core/Box';
import './Header.css';


const Header = () => { 
    return (
        
        <Box component="div" m={5} className="logoText" fontFamily="'HoneyScripts'" textAlign="center"> 
               To jest <Box component="span" color="red">logo</Box>
        </Box>
    );
};

export default Header;
