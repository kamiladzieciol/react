import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import './Images.css';


const Article = (props) => {
    const { alt, imgclass, content } = props;
    return (
    <Box component="div" m={1}>

        <Box component="div">
            <img alt={props.alt} className={props.imgclass} src={'https://via.placeholder.com/250'} />
        </Box>

        <Box component="div" >
            <Typography variant={props.variant}>{props.content}</Typography>
        </Box>

    </Box>
    );
}

Article.defaultProps = {
    alt: "Opis obrazka",
    imgclass: "img-responsive",
    content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type."
  };

export default Article;
