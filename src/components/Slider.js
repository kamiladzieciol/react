import React from 'react';
import Box from '@material-ui/core/Box';
import './Images.css';

const Slider = (props) => {
    const { alt, imgclass } = props;
    return (
        <Box component="div" m={1}> 
            <img alt={props.alt} className={props.imgclass} src={'https://via.placeholder.com/1150x350'} />
        </Box>
    ); 
};

Slider.defaultProps = {
    alt: "Opis obrazka",
    imgclass: "img-responsive"
  };

  
export default Slider;
