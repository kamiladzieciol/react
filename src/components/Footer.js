import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  footer_container: {
      backgroundColor: '#000',
      color: '#fff',
      marginTop: theme.spacing(3),
      padding: theme.spacing(3),
  },
}));


class FooterAdditional extends React.Component {
  render () {
    return (
        this.props.list.map((listValue, id)=>(
          <ListItem key={id}>{listValue}</ListItem>
        ))
    );
  }
}

class FooterLinks extends React.Component {   
      state={footerLinks: ['About Us', 'Delivery Info', 'Privacy Policy', 'Terms & Conditions']};
  render() {
      return (
          <div> 
            <Typography variant="h6">{this.props.name} </Typography>
                  <Hidden only="xs">
                        <List className="MuiTypography-body2">
                        {
                          this.state.footerLinks.map((FooterLinks)=>(
                            <ListItem key={FooterLinks}>{FooterLinks}</ListItem>
                          ))
                        }
                          <FooterAdditional list={this.props.additional}/>
                        </List> 
                  </Hidden>
          </div>
      );
  }
}

  const Footer = () => {
    const addclass = useStyles();
    return (
      <Grid container className={addclass.footer_container}>
         <Grid item sm={3} xs={12}><FooterLinks name="Information" additional={['Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum']}/></Grid>
         <Grid item sm={3} xs={12}><FooterLinks name="Customer Service" additional={['Lorem Ipsum']}/></Grid>
         <Grid item sm={3} xs={12}><FooterLinks name="Extras" additional={['Lorem Ipsum', 'Lorem Ipsum']}/></Grid>
         <Grid item sm={3} xs={12}><FooterLinks name="My Account" additional={[]}/></Grid>
        </Grid>
    );
  }
 

export default Footer;
