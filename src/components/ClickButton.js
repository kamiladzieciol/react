import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


const ClickButton = () => {
   return (
      <Button variant="contained" color="secondary">
          <Typography variant="body2">Lorem Ipsum</Typography>
      </Button>
 );
}

export default ClickButton;
