import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import './Images.css';
import Article from './Article';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Slider from './Slider';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
            main_container: {
                backgroundColor: 'white',
                border: '1px solid #efefef',
                marginTop: theme.spacing(3),
                padding: theme.spacing(1),
            },
        }));


const Main = () => {
    const addclass = useStyles();
    return (
        <Grid container className={addclass.main_container}>
            <Hidden only="xs">
                <Grid item xs={12}>
                    <Slider />
                </Grid>
            </Hidden>
            <Grid item xs={12}>  <Box component="div" m={1}> <Typography variant="h6">Lorem Ipsum</Typography> </Box></Grid>
        <Grid item xs={12} md={4} sm={6}>
            <Article imgclass="img-responsive rectangle"
                        variant="body2"
                        content="Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                 Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.">
            </Article>
        </Grid>

        <Grid item xs={12} md={4} sm={6}>
            <Article imgclass="img-responsive rectangle"
                    variant="body2" 
                    content="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.">
        </Article>
        </Grid>

        <Hidden smDown>
            <Grid item md={4}>
            <Article imgclass="img-responsive rectangle"
                    variant="body2"
                    content="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.">
            </Article>
            </Grid>
        </Hidden>

    </Grid>
  
    );
};

export default Main;
