import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import './ButtonAlign.css';
import Article from './Article';
import ClickButton from './ClickButton';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles(theme => ({
    section_container: {
        backgroundColor: 'white',
        border: '1px solid #efefef',
        marginTop: theme.spacing(3),
        padding: theme.spacing(1),
    },
    divider: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    }
}));


const Section = () => {
    const addclass = useStyles();
    return (
    <Grid container className={addclass.section_container}>

                <Grid item xs={12}>  
                    <Box component="div" m={1}> 
                        <Typography variant="h6">Lorem Ipsum</Typography>
                     </Box>
                </Grid>

        

                <Grid item md={3} sm={6} xs={12}>
                    <Article imgclass="img-responsive square"
                            content="Lorem Ipsum is simply dummy text of the printing and typesetting industry. ">
                    </Article>
                    <Box className="button_align" component="div" m={1}>  
                        <ClickButton 
                            content="Lorem Ipsum">
                        </ClickButton> 
                    </Box>
                </Grid>

                <Hidden smUp><Grid item xs={12}>  <Divider className={addclass.divider} variant="middle" /></Grid></Hidden>

                <Grid item md={3} sm={6} xs={12}>
                    <Article imgclass="img-responsive square"
                            content="Lorem Ipsum is simply dummy text of the printing and typesetting industry. ">
                    </Article>
                    <Box className="button_align" component="div" m={1}>  
                        <ClickButton 
                            content="Lorem Ipsum">
                        </ClickButton> 
                    </Box>
                </Grid>


                <Hidden smUp><Grid item xs={12}>  <Divider className={addclass.divider} variant="middle" /></Grid></Hidden>

                    <Hidden only="sm">
                        <Grid item md={3} sm={6} xs={12}>
                            <Article imgclass="img-responsive square"
                                    content="Lorem Ipsum is simply dummy text of the printing and typesetting industry. ">
                            </Article>
                            <Box className="button_align" component="div" m={1}>  
                                <ClickButton 
                                    content="Lorem Ipsum">
                                </ClickButton> 
                            </Box>
                        </Grid>

                <Hidden smUp><Grid item xs={12}>  <Divider className={addclass.divider} variant="middle" /></Grid></Hidden>

                         <Grid item md={3} sm={6} xs={12}>
                            <Article imgclass="img-responsive square"
                                    content="Lorem Ipsum is simply dummy text of the printing and typesetting industry. ">
                            </Article>
                            <Box className="button_align" component="div" m={1}>  
                                <ClickButton 
                                    content="Lorem Ipsum">
                                </ClickButton> 
                            </Box>
                        </Grid>
                    </Hidden>
  
     </Grid>
  
  
    );
};

export default Section;
